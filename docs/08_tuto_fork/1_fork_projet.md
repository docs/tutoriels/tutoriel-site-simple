---
author: Mireille Coilhac
title: Créer votre propre site
---

# Comment créer un site à partir d'un autre ?

## I. Recopier le site

👉 Aller sur le dépôt du site qui sert de modèle.

[Site à copier pour cours à usage général](https://forge.apps.education.fr/docs/modeles/site-web-cours-general){ .md-button target="_blank" rel="noopener" } 


👉 On va recopier le site : Cliquer sur le bouton «Créer une bifurcation» en haut à droite («fork» en anglais)

![nom image](images/site_ref_3.png){ width=95% }

👉 Remplir le champ Nom du projet, puis sélectionner le domaine avec le menu déroulant.

👉 **Absolument** cocher **Uniquement la branche par défaut main**

👉 Cocher «Public» puis cliquer sur «Bifurquer un projet»  
Au point 3. il faudra choisir un nom de groupe ou en créer un nouveau en cliquant juste en dessous sur "Créer un groupe".

??? danger "À savoir pour choisir un nom de groupe sur la forge des communs numériques éducatifs"

    La forge est un espace de liberté pour ses membres. Mais le choix du nom d'un groupe doit être fait en connaissant les points suivants :

    Si vous choisissez "Ministère" alors votre application en ligne ou site web personnel sera accessible à l'URL `ministere.forge.apps.education.fr` Cet exemple est caricatural mais vous comprenez aisément que cela puisse poser problème et créer de la confusion. De meme "Troisième", "Mathématiques" ou "Tutoriels" qui pourraient laisser à penser qu'on y retrouvera toutes les ressources de troisième et de mathématiques de la forge, ou des tutoriels concernant l'usage de la forge.

    Il faut donc personnaliser et ne pas utiliser de noms génériques pour les groupes : être le plus précis possible. Par exemple, pour le lycée Van Gogh, ne pas mettre seulement vanGogh, mais par exemple lyceeVanGoghErmont (lycée Van Gogh d'Ermont)

    👉 Respecter les règles suivantes :

    * Utiliser un nom de groupe qui soit clair et précis, et qui ne soit pas trompeur quant à l'identité ou les objectifs du groupe.
    * Ne pas choisir un nom de groupe qui pourrait créer une confusion avec un autre groupe existant ou qui pourrait nuire à la réputation d'autrui.
    * Les noms de groupe ne doivent en aucun cas contenir des éléments offensants, discriminatoires, ou qui pourraient gêner d'autres utilisateurs.
    * Les noms de groupe trop génériques, tels que "modèles" ou "cours", sont interdits /a priori/ afin de prévenir toute confusion et de maintenir une organisation claire au sein de la plateforme. Ils peuvent toutefois être utilisés, après accord par le comité de suivi (COSUI) s'ils représentent une communauté de contributeurs.

    Le comité de suivi se réserve le droit de juger qu'un nom de groupe n'est pas approprié et de demander d'en changer. Merci de votre compréhension. Certains groupes aux noms génériques existent et sont liés à l'existence d'une communauté active sur le sujet.


![nom image](images/faire_fork_2.png){ width=90% }

😀 On obtient : 

![nom image](images/new_3.png){ width=95% }

👉 Dans le menu de gauche sélectionner "Compilation" puis "Pipelines"

![pipeline](images/pipeline_3.png){ width=70% }

👉 Puis en haut à droite cliquer sur «New pipeline»

![run pipeline2](images/run_pipeline_5.png){ width=95% }

👉 Puis cliquer à nouveau sur «Exécuter le pipeline» sans rien changer à ce qui est proposé

![run pipeline3](images/run_pipeline_6.png){ width=95% }

⌛ Etre assez patient et attendre un peu. On attend pendant qu'il s'affiche build.

![build](images/build_2.png){ width=50% }

😀 Après quelques instants s'affiche : 

![passed](images/passed_3.png){ width=30% }


👉 Vous pouvez enfin cliquer sur "Déploiement" puis sur "pages" 

![rendu](images/trouver_pages.png){ width=20% }


👉 Il faut  décocher "Utiliser un domaine unique" 

Vous obsrvez que l'url proposée est très compliquée. Elle sera simplifiée après avoir décoché "Utiliser un domaine unique" puis enregistré la modification

![Décocher puis enregistrer](images/decocher_enregistrer_3.png){ width=70% }


👉 Après avoir enregistré la modification, vous obtenez une url plus simple : 

![url simple](images/url_simple_2.png){ width=50% }

C'est l'adresse de rendu de votre site, vous pouvez la noter 😊.



## II. Personnaliser le site

😀 C'est **presque** fini !


!!! info "Il faut modifier le README.md "

    👉 Cliquer sur le nom de votre site en haut à gauche

    ![clic mon site](images/mon_site.png){ width=30% }

    👉 Cliquer sur "Modifier", puis sélectionner "EDI Web"

    ![web ide](images/web_IDE_2.png){ width=95% }

    👉 Cliquer sur le fichier README.md 

    ![readme](images/fichier_readme.png){ width=30% }

    👉 Modifier le fichier README.md 

    ![fichier readme](images/modifier_readme.png){ width=60% }

    👉 Enregistrer la modification du fichier en réalisant un "commit" 

    Voir ci-dessous

!!! info "Réaliser un commit "

    Suivre cette méthode pour toutes les modifications de fichiers de votre site.

    Cliquer sur l'icône :

    ![commit](images/realiser_commit.png){ width=40% }

    Renseigner le commit :

    ![message commit](images/message_commit_1.png){ width=50% }

    Visualiser les modifications apportées par le commit demandé :

    ![visu](images/visu_1.png){ width=90% }

    Faire le commit :

    ![faire commit](images/to_main.png){ width=40% }

    En bas à droite, Cliquer sur Go to project pour visualiser l'évolution de la construction du site.

    ![retour projet](images/go_project.png){ width=40% }

    Au début, la construction est en cours :

    ![pipeline attend](images/en_cours.png){ width=90% }

    Une fois la construction terminée, on obtient : 

    ![pipeline ok](images/ok.png){ width=90% }

    Rafraichir la page du rendu de votre site. Il faut attendre un peu, et parfois recommencer, pour voir apparaître les modifications.



!!! warning "Personnaliser le projet : Modifier le fichier mkdocs.yml"

    👉 Revenir sur Web IDE

    ![web ide](images/web_IDE_2.png){ width=90% }

    👉 Sélectionner le fichier mkdocs.yml

    ![fichier mkdocs.yml](images/mkdocs_2.png){ width=25% }

    👉 Modifier le fichier mkdocs.yml
    
    Il y a seulement deux lignes à modifier :

    * Ligne :   
    `site_description: Un modèle avec mkdocs`  
    Remplacer `Un modèle avec mkdocs` par votre propre description de site (par exemple `Cours de SVT de ...` )

    * Sous la ligne :  
    `copyright: |`  
    Remplacer `Nom d'auteur` par votre nom

    👉 Faire le commit

    ??? "Le nom du site"

        Le nom du site est le nom du projet que vous avez donné lorsque vous avez rélisé la bifurcation du site modèle.


!!! info "Modifier le fichier index.md"

    👉 Revenir sur Web IDE

    ![web ide](images/web_IDE_2.png){ width=90% }

     👉 Sélectionner le fichier `index.md` du dossier `docs`

    ![index](images/index_2.png){ width=30% }

    * Éditer le fichier `index.md`
    * Le personnaliser. Il s'agit de la page d'accueil de votre site.

    Si besoin voir le tutoriel [Avant de démarrer](../01_demarrage/1_demarrage.md){:target="_blank" }


    👉 Faire le commit
    

!!! abstract "URL du site"

    😀 On peut se rendre sur l'adresse du rendu (nous l'avons trouvée dans "Déploiement" puis "Pages") pour voir le résultat.


!!! info "Changer de logo"

    Vous pouvez choisir un autre logo en suivant ce lien : [Logos Material Design Icons](https://pictogrammers.com/library/mdi/){ .md-button target="_blank" rel="noopener" }

    * Comme expliqué précédemment, ouvrir le fichier `mkdocs.yml`
    * Vers la ligne 25 vous pouvez lire :  
    `logo: material/stairs-up`
    
    👉 Il suffit de remplacer `stairs-up` par la référence choisie.

    Par exemple : 

    avec : `logo: material/teddy-bear` 
    
    on obtient le rendu suivant : 

    ![logo ours](images/ours_2.png){ width=20% }




## III. Votre propre contenu

Pour ajouter vos propres pages, voir le tutoriel [Kit de survie Gitlab en ligne](../10_survie/kit_gitlab.md){:target="_blank" }


??? tip "⚙️ Modification avancée de votre site : supprimer le lien vers le dépôt du site"

    ⚠️ Cette modification n'est à réaliser que si vous avez bien réfléchi aux conséquences. 

    Elle **supprime** le lien direct de votre site 

    * vers le dépôt du site : ![forge](images/forge.png){ width=6% }

    * vers le dépôt de la page active : ![modif_page](images/modif_page.png){ width=5% }

    👉 Vous pourrez toujours atteindre votre dépôt à partir de son adresse url.

    👉 Ci-dessous votre haut de page avant et après la modification :

    Avant : ![avant](images/avant_modif.png){ width=30% } 
    
    Après : ![après](images/apres_modif.png){ width=30% }

	??? note "Comment faire ?"

    	* Suivre les explications du II. pour ouvrir le fichier `mkdocs.yml`

    	* Vers la ligne 58 mettre en commentaire (ajouter simplement `#` au début de la ligne) les deux lignes suivantes.

      	`# repo_url: !ENV [CI_PROJECT_URL]`  
		`# edit_uri: !ENV [EDIT_VARIABLE]`

        👉 Faire le commit
		

        
        !!! warning "Ne pas supprimer"

            Il est recommandé de **ne pas supprimer** ces deux lignes pour pouvoir les rétablir facilement en supprimant le `#` du début de la ligne.
		


