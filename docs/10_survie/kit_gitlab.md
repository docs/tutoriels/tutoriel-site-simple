---
author: Mireille Coilhac
title: Kit de survie GitLab en ligne
---

🌵 Comment créer mon contenu sur mon beau site tout neuf ?



!!! danger "Attention"

    * Ce tutoriel est destiné aux grands débutants, ne connaisssant pas du tout GitLab.
    * Il ne présente que le strict minimum pour être autonome dans la création d'un site.


!!! info "Travailler en ligne"

	L'IDE utilisée en ligne est VS Code Web.

    ![web IDE](images/web_IDE_2.png){ width=90% }

	Pour composer vos documents, on peut écrire directement dans l'éditeur WEB IDE de la forge et utiliser le bouton «Créer un commit» comme un bouton sauvegarder. La différence par rapport à une sauvegarde «classique» est que vous êtes invités à commenter la ou les modifications apportées afin de créer un historique des modifications successives. Pour les utilisateurs avancés, on peut travailler localement, mais cela sera précisé dans un autre tutoriel.


!!! info "Configurer la tabulation"

	En bas à droite, cliquer sur "Select Indentation"

	![selectionner indentation](images/espaces.PNG){ width=40% }

	Cliquer ensuite sur "Indent Using Spaces". Sur le menu déroulant qui s'ouvrira, sélectionner 4.

	![indentations](images/indent.png){ width=40% }



!!! danger "Attention : noms de répertoires et noms de fichiers"

	Eviter les accents, caractères spéciaux, espaces. Pour séparer les éléments de nommage, utiliser les underscores :  
	Par exemple : écrire `chapitre_intro`

	

## I. Ajouter un répertoire dans docs

![new_folder](images/new_folder_2.png){ width=70% }

![repertoire](images/repertoire.png){ width=50% }

!!! info "Glisser-déposer"

	On peut aussi tout simplement glisser-déposer un répertoire ou un fichier à l'endroit voulu.



## II. Ajouter un répertoire dans un autre

Vous aurez souvent besoin d'ajouter d'autres répertoires comme :

* `images`
* `a_telecharger`
* `scripts`

![nouveau répertoire](images/nouv_repert_2.png){ width=80% }

![rens](images/rens_rep_new.png){ width=60% }

!!! info "Glisser-déposer"

	On peut aussi tout simplement glisser-déposer un répertoire ou un fichier à l'endroit voulu.


## III. Créer en ligne un fichier .md ou .py

!!! info "Editeur de texte intégré pour créer des fichiers `.md` ou `.py`"

	Sur le répertoire désiré, faire un clic droit, puis sélectionner **New File**

	![nouveau fichier](images/nouveau_fichier_3.png){ width=40% }

	Saisir le nom du fichier que vous désirez créer, sans oublier son extension : par exemple `cours_1.md`. 

	![nom du fichier](images/nom_fichier.png){ width=30% }

	Appuyer sur la touche "entrée" <kbd>↵</kbd> du clavier pour créer le fichier. Vous pouvez maintenant écrire dans la partie éditeur, à droite.

	![ecrire](images/ecrire.png){ width=50% }

	😊 Ce procédé est utilisable aussi pour écrire un fichier `.py` par exemple.

    👉 Vous pouvez aussi travailler hors ligne et utiliser un éditeur de texte comme Notepad++  pour créer vos fichiers `.md`, puis les télécharger ensuite dans votre dépôt.

	??? tip "Visualiser votre fichier .md"

    	![Ouvrir la prévisualisation](images/open_preview.png){ width=90% }

		![Visualisation du fichier md](images/view_md.png){ width=90% }


!!! info "Comment écrire vos pages"

    👀 Vous pouvez explorer toutes les autres rubriques de ce tutoriel ...  
	
	😊 Bonne lecture ...

	* Pour les admonitions, liens, vidéos, QCM, exercices, graphes, arbres : 
	[une page basique](../02_basique/2_page_basique.md){:target="_blank" }
	* Pour les  [formules en mathématiques et chimies](../latex/formules_latex.md){:target="_blank" }
	* Pour les [tags](../04_tags/mettre_tags.md){:target="_blank" }
	* Pour [les fichier pdf](../pdf/creer_pdf.md)



## IV. Télécharger un fichier

Vous pourrez ainsi télécharger vos fichiers `.md`, `.py`, `.jpg`, `.ipynb`, etc.

![nouveau fichier](images/upload.png){ width=80% }


## V. Faire un commit pour un nouveau répertoire, le téléchargement de fichiers, la création d'un fichier

!!! danger "Attention"

    Pour qu'une modification soit réellement effectuée (création de répertoire, modification de fichier ...) il faut faire un commit.

    On peut réaliser un seul commit pour plusieurs actions effectuées. **C'est même recommandé**, par soucis d'économie. Faire un commit est en effet énergétiquement coûteux. 

!!! danger "⏳ Attention ... savoir patienter entre deux commits ..."

    La construction du site après un commit prend quelques instants. Il est vivement conseillé de vérifier sur le rendu du site que les modifications ont été prises en compte, **avant** de se lancer dans un nouveau commit. Il est parfois nécessaire de rafraichir plusieurs fois la page de rendu du site pour voir les modifications réalisées.   


!!! info "Commit pour nouveau répertoire, téléchargement fichier, ou toute modification sur votre site"

	!!! danger "Attention"

		Il **ne faut pas** faire un commit pour un répertoire qui a été créé, mais est encore vide.  
		
		😨 Ce répertoire serait **supprimé** après le commit.

	👉 Suivre cette méthode pour toutes les modifications sur votre site.

	👉 Sur la barre d'outil latérale gauche :

	![pour commit](images/pour_commit.png){ width=20% }

	![texte commit](images/texte_commit.png){ width=50% }

	👉 Faire le commit :

    ![faire commit](images/to_main.png){ width=40% }

    👉 En bas à droite, Cliquer sur Go to project pour visualiser l'évolution de la construction du site.

    ![retour projet](images/go_project.png){ width=40% }
    Au début, la construction est en cours :

    ![pipeline attend](images/en_cours.png){ width=95% }

    👉 Une fois la construction terminée, on obtient : 

    ![pipeline ok](images/ok.png){ width=90% }

    👉 Rafraichir la page du rendu de votre site. Il faut attendre un peu, et parfois recommencer, pour voir apparaître les modifications.
	

???+ note "Les commits"

    Un commit, c’est la sauvegarde des modifications, accompagnée d'un commentaire qui résume les modifications effectuées.


!!! info "Supprimer un commit : 'faire un revert'"

    Vous pouvez vouloir supprimer un commit pour plusieurs raisons : 
    
    * le pipeline n'est pas en échec, mais vous n'avez pas fait ce que vous vouliez
    * il a mis le pipeline en échec

    👉 Le plus simple est de faire un nouveau commit qui corrige ce que vous voulez.

    ??? note "supprimer des commits"

        Si vous ne voulez pas faire un nouveau commit, et voulez vraiment réaliser des suppressions de commits, voici comment procéder.

		!!! warning "Remarque pour plusieurs commits"

			Pour supprimer plusieurs commits, il faut les supprimer **un à un**, en suivant pour chacun la procédure indiquée ci-dessous.
		

        ![historique](images/historique_2.png){ width=80% }

        ![commit à supprimer](images/commit_a_supprimer.png){ width=80% }

        ![defaire](images/defaire.png){ width=80% }

        ![confirmer défaire](images/confirm_defaire_2.png){ width=60% }

        Il s'affiche alors :

        ![Revert réussi](images/revert_reussi_2.png){ width=80% }


??? info "Visualiser un commit"

	![menu gestion](images/voir_activite.png){ width=40% }

	![numero du commit](images/numero.png){ width=40% }

	![vue du commit](images/vue_commit.png){ width=90% }





## VI. Modifier un fichier


!!! info "Modifier un fichier"

	![a_modifier](images/a_modif.png){ width=90% }

	Réaliser les modifications

	![modifs](images/modifs.png){ width=90% }



!!! info "Commit pour modification de fichier"

	On procède comme précédemment.

	![pour commit](images/pour_commit.png){ width=20% }


	Observer les modifications

	![visu modifs](images/visu_3.png){ width=80% }

	Si les modifications conviennent

	![faire push](images/to_main.png){ width=40% }

	Procéder ensuite comme au IV.


## VII. Renommer un fichier

![renommer](images/renommer.png){ width=70% }

!!! danger "Attention"

    Si vous avez renommé un fichier (ou un répertoire) qui était écrit dans un fichier **`.pages`**, modifier ce fichier **`.pages`** avec le nouveau nom, **avant** de réaliser le commit.  



## VIII. L'interface Web IDE et le dépôt

Pour revenir sur le dépôt (⚠️ attention ne pas oublier de faire un commit s'il y a eu une modification, avant de revenir sur le dépôt)

Tout en bas à gauche :

![pour depot 1](images/pour_depot_1.png){ width=20% }

Puis tout en haut :

![pour depot 2](images/pour_depot_2.png){ width=30% }











