---
author: Mireille Coilhac
title: Les fichiers pdf
---

## I. Afficher un fichier pdf

!!! info "Télécharger vos fichiers pdf"

    Commencer par télécharger vos fichiers pdf dans un répertoire `a_telecharger` de votre rubrique.


!!! info "Méthode avec un lien qui s'ouvre dans une autre fenêtre"

    ```markdown title="Le code à copier"
    [Flash 3A : lire un nombre dérivé](a_telecharger/flash3A.pdf){ .md-button target="_blank" rel="noopener" }
    ```
    <div class="result" markdown>
    [Flash 3A : lire un nombre dérivé](a_telecharger/flash3A.pdf){ .md-button target="_blank" rel="noopener" }  
    </div>

!!! info "Méthode avec un iframe"

    ```markdown title="Le code à copier"
	<div class="centre">
	<iframe 
	src="../a_telecharger/flash3A.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>
    ``` 

    <div class="result" markdown>

    <div class="centre">
	<iframe 
	src="../a_telecharger/flash3A.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>

    </div>


    !!! warning "Remarque"

        Cette façon de procéder par iframe est plus "lourde" que celle avec un lien qui s'ouvre dans une autre fenêtre.
        

!!! info "Méthode avec un iframe dans une admonition"

    Vous pouvez utiliser cette méthode à l'intérieur d'une admonition qui se déroule en cliquant dessus.

    ```markdown title="Le code à copier"
    ??? note "Le fichier pdf"
	    <div class="centre">
	    <iframe 
	    src="../a_telecharger/flash3A.pdf"
	    width="1000" height="1000" 
	    frameborder="0" 
	    allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	    </iframe>
	    </div>
    ``` 

    <div class="result" markdown>

    ??? note "Le fichier pdf"
        <div class="centre">
	    <iframe 
	    src="../a_telecharger/flash3A.pdf"
	    width="1000" height="1000" 
	    frameborder="0" 
	    allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	    </iframe>
	    </div>

    </div>

## II. Créer un fichier pdf à partir d'un fichier markdown

!!! info "Pourquoi créer un fichier  pdf ?"

    Vous pouvez désirer pouvoir disposer de traces "papier" de certaines pages de votre site.

    Transformer une page `bilan_cours_1.md` en `bilan_cours_1.pdf` permettra de garder une trace écrite.

    Cela peut être utile pour créer rapidement des fichiers bilans "à trous"


!!! warning "Quel navigateur ?"

    Ce tutoriel a été créé avec le navigateur Firefox, mais la démarche est analogue avec d'autres.


👉 Sur la page du site que vous désirez convertir en fichier pdf, cliquer dans la barre d'outil du navigateur sur le menu.

![menu_imprimer.png](images/pour_imprimer.png){ width=90% }

* Puis cliquer sur imprimer.

![imprimer.png](images/imprimer.png){ width=20% }

* Dans le menu déroulant Destination, sélectionner print to PDF, puis cliquer sur Plus de paramètres.

![menu_imprimer.png](images/print_to_pdf.png){ width=20% }

* Choisir les options 
    * Choix des marges.
	* Format Original.
	* Décocher Imprimer les en-têtes et pieds de page.

![options.png](images/options.png){ width=20% }


👉 Cliquer sur imprimer, puis enregistrer dans le répertoire souhaité.



## III. Convertir un fichier markdown dans un autre format.

	[Pandoc en ligne](https://pandoc.org/try/){ .md-button target="_blank" rel="noopener" }





