---
author: Mireille Coilhac
title: Ressources
---


[Tutoriel pour créer son site avec Python intégré](https://docs.forge.apps.education.fr/modeles/tutoriels/pyodide-mkdocs-theme-review/){ .md-button target="_blank" rel="noopener" }

[Catalogue de ressources par Nathalie Bessonnet](https://bessonnetnathalie.forge.apps.education.fr/ressources-nsi/){ .md-button target="_blank" rel="noopener" }


