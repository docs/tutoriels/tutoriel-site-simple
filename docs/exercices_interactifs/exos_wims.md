---
author: Mireille Coilhac
title: Exercices interactifs
---

Le site WIMS met à disposition beaucoup de ressources interactives dans de nombreux domaines : Biologie, Chimie, Informatique, Langues, Mathématiques, Physique, et autres domaines encore (disciplines artistiques, Géographie, Sciences de la terre, Histoire, Sciences, Ecogestion, Éducation physique, Astronomie, Sciences et techniques industrielles)

[Contenu pédagogique de WIMS](https://euler-ressources.ac-versailles.fr/wims/wims.cgi?session=9G03F5B8EF_help.3&+lang=fr&+module=adm%2Fbrowse&+cmd=new&+job=subject){ .md-button target="_blank" rel="noopener" }


## Chercher un exercice WIMS

!!! info "En regardant les thèmes proposés"

    * Suivre le lien suivant : [Contenu pédagogique de WIMS](https://euler-ressources.ac-versailles.fr/wims/wims.cgi?session=9G03F5B8EF_help.3&+lang=fr&+module=adm%2Fbrowse&+cmd=new&+job=subject){:target="_blank" }
    * Sélectionner l'onglet voulu
    * Cliquer sur le thème choisi puis **descendre au bas de la page** et explorer ce qui est proposé dans l'onglet "Modules d'exercices"

!!! info " En utilisant la barre de recherche"

    * Suivre le lien suivant : [Recherche dans WIMS](https://euler-ressources.ac-versailles.fr/wims/){:target="_blank" }
    * Descendre au bas de la page. En dessous des cadres "Actualités-Exemples", compléter la barre de recherche.

    ![recherche wims](images/recherche_wims.png){ width=70% }

    * Les résultats apparaîssent en bas de page.

## Intégrer un exercice dans votre site

* Cliquer sur un module d'exercices dans l'onglet "Modules d'exercices".
* Dans le cadre "Choix des exercices", cliquer sur l'exercice voulu. On intègre dans le site **un seul exercice à la fois**.
* Modifier si nécessaire les paramétrages proposés en dessous.
* Tout en bas Cliquer sur le bouton <kbd>Au travail</kbd>.
* La page de l'exercice choisi apparaît. En haut à gauche cliquer sur "À propos".

![A propos](images/a_propos_wims.png){ width=30% }

* Dans la colonne de gauche du cadre "Informations sur cet exercice", à la ligne "Lecteur exportable" , recopier tout ce qui se trouve dans le paragraphe : 
"Afficher la config actuelle  sur un site / un blog ("light" version):"

![iframe](images/iframe_wims.png){ width=50% }

!!! info "Exemple d'exercice de géographie"

    ```markdown title="Le code à copier"
    <iframe src="https://euler-ressources.ac-versailles.fr/wims/wims.cgi?module=adm/raw&job=lightpopup&emod=E4/language/oefgeog.fr&parm=cmd=new;exo=payseurop;qnum=1;scoredelay=;seedrepeat=0;qcmlevel=1&option=noabout" width="100%" height="600"></iframe>
    ```
    <div class="result" markdown>
    <iframe src="https://euler-ressources.ac-versailles.fr/wims/wims.cgi?module=adm/raw&job=lightpopup&emod=E4/language/oefgeog.fr&parm=cmd=new;exo=payseurop;qnum=1;scoredelay=;seedrepeat=0;qcmlevel=1&option=noabout" width="100%" height="600"></iframe>
    </div>

## Documentation officielle de WIMS

[Documentation officielle de WIMS](https://nuage03.apps.education.fr/index.php/s/yrRk8HmxjmfAzf6){ .md-button target="_blank" rel="noopener" }




