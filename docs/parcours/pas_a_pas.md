---
author: Mireille Coilhac
title: 👣 Parcours pas à pas
---

!!! info "Pourquoi ce parcours ?"

    Dans ce parcours "pas à pas", vous allez acquérir un peu de vocabulaire spécifique, apprendre à recopier le site 
    modèle proposé, à l'adapter, en utilisant Web IDE.

    Dans un deuxième temps, vous pourrez prendre un peu de temps pour parcourir et découvrir les différentes pages de ce site.

## I. Le rendu

Vous trouverez ici un exemple de ce que vous pouvez réaliser. Prenez un peu de temps pour parcourir les différentes pages : 

[Rendu du site simple](https://docs.forge.apps.education.fr/modeles/site-web-cours-general/){ .md-button target="_blank" rel="noopener" }

## II. Explications et vocabulaire

Le rendu de votre site sera construit **automatiquement**. Nous allons voir comment procéder.

* Tout d'abord vous trouverez ici une présentation de la structure du site qui a permis le rendu que vous venez de consulter : 
[La structure du site](https://docs.forge.apps.education.fr/tutoriels/tutoriel-site-simple/01_demarrage/1_demarrage/#i-la-structure-du-site){:target="_blank" }

* Il faut connaître aussi un peu de [vocabulaire](https://docs.forge.apps.education.fr/tutoriels/tutoriel-site-simple/01_demarrage/1_demarrage/#ii-le-vocabulaire){:target="_blank" }

## III. C'est parti pour créer votre propre site

### Création à partir du site modèle

[Votre propre site](../08_tuto_fork/1_fork_projet.md){ .md-button target="_blank" rel="noopener" }

### Créer votre première page

Pour cela, vous allez travailler sur GitLab en ligne.

* Vous devrez suivre le I, II et III ici : [Kit de survie GitLab en ligne](../10_survie/kit_gitlab.md){:target="_blank" }

Le début de votre premier fichier `ma_premiere_page.md` ressemblera à ceci : 

```markdown title="Code à copier au début d'un fichier .md"
---
author: compléter avec les noms d'auteurs
title: Compléter le titre qui sera affiché dans le menu
---

Ma jolie page
```

* Amusez-vous à ajouter une [admonition](https://docs.forge.apps.education.fr/tutoriels/tutoriel-site-simple/02_basique/2_page_basique/#ii-les-admonitions)

* [Faire un commit](https://docs.forge.apps.education.fr/tutoriels/tutoriel-site-simple/10_survie/kit_gitlab/#v-faire-un-commit-pour-un-nouveau-repertoire-le-telechargement-de-fichiers-la-creation-dun-fichier){:target="_blank" }

⚠️ Si vous regardez le rendu de votre site, la nouvelle page n'apparaît sûrement 
pas. Il faut en effet modifier le fichier `.pages`



* Faites apparaître votre page dans le menu : [Structure du site et organisation du menu](../09_pages/pages.md){:target="_blank" }


* Réaliser à nouveau un commit, puis observer le rendu de votre site, qui contient maintenant votre nouvelle page.  
Ce n'est pas instantanné, il faut parfois rafraîchir le site.

### Cacher les pages du site modèle

Vous pouvez vouloir garder ces pages, comme modèles, sans qu'elles ne soient accessibles par votre menu. Il suffit de
les "sortir" du fichier .pages principal : 

=== "Fichier .pages du modèle"
    ```markdown title="Fichier .pages du site modèle"
    nav:
        - index.md
        - chapitre01
        - chapitre_plusieurs_pages
        - qcm_1
        - tags.md
        - credits.md
    ```

=== "Votre fichier .pages"
    ```markdown title="Fichier .pages de votre site"
    nav:
        - index.md
        - ma_premiere_page.md
        - tags.md
        - credits.md
    ```

Il ne restera que l'accueil, votre page, les tags et les crédits.


## III. S'initier un peu plus à Web IDE

Votre site est parti 😊.  
Pour poursuivre, vous aurez besoin d'en apprendre un peu plus sur GitLab en ligne : 
[à partir du VI du kit de survie GitLab](../10_survie/kit_gitlab.md){:target="_blank" }

## IV. Un contenu plus élaboré

!!! info "Comment écrire vos pages"

    👀 Vous pouvez explorer toutes les autres rubriques de ce tutoriel ...  
	
	😊 Bonne lecture ...

	* Pour les admonitions, liens, vidéos, QCM, exercices, graphes, arbres, etc : 
	[une page basique](../02_basique/2_page_basique.md){:target="_blank" }
	* Pour les  [formules en mathématiques et chimies](../latex/formules_latex.md){:target="_blank" }
	* Pour les [tags](../04_tags/mettre_tags.md){:target="_blank" }
	* Pour [les fichier pdf](../pdf/creer_pdf.md)